package com.company;

public class Cutting {
    private int product_count;

    public Cutting(int product_count) {
        check(product_count);

    }

    public Cutting() {
        this.product_count=0;
    }

    public int increaseProduct(){
        product_count=product_count+1;
        System.out.println("Number of cut product  : "+product_count);
        return product_count;
    }
    public int decreaseProduct(){
        product_count=product_count-1;
        check(product_count);
        System.out.println("Number of cut product  : "+product_count);
        return product_count;


    }
    private void check(int count){
        if(count<0){
            this.product_count= 0;
        }
        else{
            this.product_count=count;
        }
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        check(product_count);
    }
}
