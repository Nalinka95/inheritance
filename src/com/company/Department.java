package com.company;

public interface Department {
     int decreaseProduct();
     int increaseProduct();
}
