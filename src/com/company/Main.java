package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Cutting cut_1 =new Cutting();
        Cutting cut_2 = new Cutting(cut_1.increaseProduct());
        cut_2.increaseProduct();

        Sewing sew_1= new Sewing(10);
        Sewing sew_2= new Sewing(sew_1.decreaseProduct());
        sew_2.decreaseProduct();

        Packing pack_1= new Packing(-10);
        pack_1.decreaseProduct();
        Packing pack_2= new Packing(10);
        pack_2.increaseProduct();

    }
}
