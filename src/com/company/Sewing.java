package com.company;

public class Sewing implements Department {
    private int product_count;

    public Sewing(int product_count) {
        check(product_count);

    }

    public Sewing() {
        this.product_count=0;
    }

    public int increaseProduct(){
        product_count=product_count+5;
        System.out.println("Number of Sewing product  : "+product_count);
        return product_count;
    }
    public int decreaseProduct(){
        product_count=product_count-5;
        check(product_count);
        System.out.println("Number of Sewing product  : "+product_count);
        return product_count;


    }
    private void check(int count){
        if(count<0){
            this.product_count= 0;
        }
        else{
            this.product_count=count;
        }
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        check(product_count);
    }
}
